/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

const myHeaders = new Headers();
//myHeaders.append("Content-Type", "text/plain");

const url = "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";
const urlFriends = "http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2";

//+ Бонус.
const ConvertToJSON = (data) => data.json();

fetch(url, { method: "POST", headers: myHeaders })
  .then(ConvertToJSON)
  .then(res => {
    let randomPerson = res[Math.random() * res.length | 0];
    return randomPerson;
  })
  .then(randomPerson => {
    return fetch(urlFriends, { method: "POST", headers: myHeaders })
      .then(ConvertToJSON)
      .then(res => {
        let newPerson = {
          name: randomPerson.name,
          age: randomPerson.age,
          phone: randomPerson.phone,
          friends: res[0].friends
        }
        showPerson(newPerson)
      })
  }
  )

//5. Вывести результат на страничку
function showPerson(person) {
  let resultView = document.getElementById("parseObjDiv");
  resultView.innerText = JSON.stringify(person, null, '\t');
}

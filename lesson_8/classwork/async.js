/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

const url = "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";


async function getCompanies() {
  const getCompaniesResponse = await fetch(url)
  const companies = await getCompaniesResponse.json();

  //Create Table
  const heading = getHeaders();
  const dataForTable = getDataForTable(companies);
  console.log(heading);
  console.log(dataForTable);

  let myTableDiv = document.getElementById("parseObjDiv")
  addTable(myTableDiv, heading, dataForTable);
}

getCompanies();

function getHeaders() {
  var headingDictionary = {};
  headingDictionary["id"] = "#"
  headingDictionary["name"] = "Company"
  headingDictionary["balance"] = "Balance"
  headingDictionary["registered"] = "Показать дату регистрации"
  headingDictionary["address"] = "Показать адресс"
  return headingDictionary;
}

let _lastId = 1;
function getDataForTable(companies) {
  let companiesList = new Array();
  companies.forEach(el => {
    let company = {
      id: _lastId++,
      name: el.company,
      balance: el.balance,
      registered: { text: el.registered.substring(0, 10), hide: "hide" },
      address: { text: el.address, hide: "hide" }
    }
    companiesList.push(company);
  })
  return companiesList;
}



async function addTable(parentElement, heading, objectArray) {
  var table = document.createElement('TABLE')
  var tableBody = document.createElement('TBODY')

  table.border = '1'
  table.appendChild(tableBody);

  //TABLE COLUMNS
  var tr = document.createElement('TR');
  tableBody.appendChild(tr);

  for (var key in heading) {
    var th = document.createElement('TH');
    th.width = '75';
    th.appendChild(document.createTextNode(heading[key]));
    tr.appendChild(th);
  }

  //TABLE ROWS
  objectArray.forEach(el => {
    var tr = document.createElement('TR');
    for (var key in el) {
      var td = document.createElement('TD')

      let element = el[key];
      if (element === Object(element) & element["hide"] === "hide") {
        td.innerText = "Hide value (click)";
        td.style.color = "blue";
        td.onclick = (e) => {
          e.target.innerText = element["text"];
        }
      }
      else { td.innerText = element; }
      tr.appendChild(td)
    }
    tableBody.appendChild(tr);
  })
  parentElement.appendChild(table)
}

// Help
function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}
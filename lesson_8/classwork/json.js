
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <input />

  -> '{"name" : !"23123", "age": 15, "password": "*****" }'
*/
document.addEventListener('DOMContentLoaded', () => {

  const myUploadForm = document.forms.parseForm;
  const btnCreateObj = document.getElementById("btnCreateObj");
  const parseObjDiv = document.getElementById("parseObjDiv");
  const myInput = document.getElementById('myInput');
  let parserObj = {};

  myUploadForm.onsubmit = function (e) {
    e.preventDefault();
  }
  btnCreateObj.addEventListener('click', btnCreateClick);
  myInput.addEventListener('change', changeText);

  function btnCreateClick(event) {
    Array.from(parseForm.elements).forEach(element => {
      if (element.type === 'text') {
        parserObj[element.name] = element.value;
      }
    })

    var text = JSON.stringify(parserObj);
    parseObjDiv.innerText = text;
    console.log(text);
  }

  function changeText(event){
    var text = event.target.value;
    var reparseObj = JSON.parse(text)
    console.log(reparseObj);
  }
});

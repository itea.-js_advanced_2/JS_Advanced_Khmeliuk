/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>
*/

document.addEventListener("DOMContentLoaded", function () {
  let commentBase = {
    likesCount: 0,
    avatarUrl: "https://ddragon.leagueoflegends.com/cdn/9.4.1/img/champion/Olaf.png",
    Like
  }

  function Like() {
    this.likesCount++;
  }

  function Comment(name, text, avatarUrl) {
    this.name = name;
    this.text = text;

    Object.setPrototypeOf(this, commentBase);

    if (avatarUrl !== undefined) {
      this.avatarUrl = avatarUrl;
    }
  }

  let comment1 = new Comment("Blitzcrank", "comment1", "https://ddragon.leagueoflegends.com/cdn/9.4.1/img/champion/Blitzcrank.png");
  let comment2 = new Comment("Katarina", "comment2", "https://ddragon.leagueoflegends.com/cdn/9.4.1/img/champion/Katarina.png");
  let comment3 = new Comment("Gragas", "comment3", "https://ddragon.leagueoflegends.com/cdn/9.4.1/img/champion/Gragas.png");
  let comment4 = new Comment("Jinx", "comment4", "https://ddragon.leagueoflegends.com/cdn/9.4.1/img/champion/Jinx.png");
  let comment5 = new Comment("Cait", "comment5");

  comment1.Like();
  comment1.Like();
  comment1.Like();
  comment2.Like();
  comment2.Like();

  let commentsArray = [comment1, comment2, comment3, comment4, comment5];

  function printComments(array) {
    array.forEach(comment => {
      commentDispley(comment);
    });
  }

  const main = document.getElementById("bodyId");
  
  function commentDispley(comment){
    // оболочка
    let mainDiv = document.createElement("div");
      mainDiv.classList.add("mainDiv");
    // иконка
    let img = document.createElement("img");
      img.width = 64;
      img.height = 64
    let picDiv = document.createElement("div");
      picDiv.setAttribute("id", "pic");
    //Div текста
    let textDiv = document.createElement("div");
      textDiv.setAttribute("id", "text");  
    //имя
    let name = document.createElement("h2");
    //текст
    let text = document.createElement("em");
    let like = document.createElement("p");
    like.setAttribute("align", "right");
  
    picDiv.appendChild(img);
    textDiv.appendChild(name);
    textDiv.appendChild(text);
    textDiv.appendChild(like);
    mainDiv.appendChild(picDiv);
    mainDiv.appendChild(textDiv);

    // set values
    img.src = comment.avatarUrl;
    name.innerHTML = comment.name;
    text.innerHTML = comment.text;
    like.innerHTML = "likes " + comment.likesCount;
    
    main.appendChild(mainDiv);
  }
  
  printComments(commentsArray);
})
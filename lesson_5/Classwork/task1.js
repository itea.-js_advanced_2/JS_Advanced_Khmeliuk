/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
let train = {};
train.name = "testName";
train.speed = 0;
train.personsCount = 5;

train.Run = function (speed) {
    this.speed = speed;
    console.log("Поезд " + this.name + " везет " + this.personsCount + " со скоростью " + this.speed);
    //alert("Поезд " + this.name + " везет " + this.personsCount + " со скоростью " + this.speed);
}
train.Stop = function (speed) {
    this.speed = speed;
    console.log("Поезд " + this.name + " остановился. Скорость " + this.speed + "")
}
train.GetPersons = function (speed, personsCount) {
    this.speed = speed;
    this.personsCount += personsCount;
    console.log("speed = " + this.speed + "; person count = " + this.personsCount);
}

train.Run(20);
train.Stop(0);
train.GetPersons(0, 5);


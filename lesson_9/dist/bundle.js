/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./classwork/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./classwork/app.js":
/*!**************************!*\
  !*** ./classwork/app.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _js_Dove__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/Dove */ \"./classwork/js/Dove.js\");\n/* harmony import */ var _js_Owl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/Owl.js */ \"./classwork/js/Owl.js\");\n\r\n\r\n\r\n\r\nlet owl = new _js_Owl_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"](20, \"grey\");\r\nowl.Hunt(\"мышь\");\r\nowl.Fly(20);\r\nowl.Eat(15);\r\nowl.Scurry();\r\nowl.Sing(\"уу-уу\");\r\n\r\nconsole.log(\"\");\r\n\r\nlet dove = new _js_Dove__WEBPACK_IMPORTED_MODULE_0__[\"Dove\"](10, \"grey\");\r\ndove.Fly(15);\r\ndove.Eat(45);\r\ndove.Scurry();\r\ndove.Sing(\"ур-ур-ур\");\r\n\n\n//# sourceURL=webpack:///./classwork/app.js?");

/***/ }),

/***/ "./classwork/js/Bird.js":
/*!******************************!*\
  !*** ./classwork/js/Bird.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Bird; });\nclass Bird {\r\n    constructor(kind, isPredator, size, color) {\r\n      this.isPredator = isPredator;\r\n      this.color = color;\r\n      this.kind = kind;\r\n      this.size = size;\r\n    }\r\n  \r\n    // - Нестись\r\n    Scurry() {\r\n      console.log(`Опа, яйцо`);\r\n    }\r\n    // //- Плавать\r\n    // Swim = () => {\r\n  \r\n    // }\r\n    //- Кушать\r\n    Eat(eat) {\r\n      if (eat > 20) {\r\n        console.log(`птица сыта и больше есть не может!`);\r\n      }\r\n      else {\r\n        console.log(`ням ням, было вкусно!`);\r\n      }\r\n    }\r\n    //- Петь\r\n    Sing(sound) {\r\n      console.log(`${this.kind} издает ${sound}`);\r\n    }\r\n    // //- Переносить почту\r\n    // SendMail = () => {\r\n  \r\n    // }\r\n    // //- Бегать\r\n    // Run = () => {\r\n  \r\n    // }\r\n  }\n\n//# sourceURL=webpack:///./classwork/js/Bird.js?");

/***/ }),

/***/ "./classwork/js/Dove.js":
/*!******************************!*\
  !*** ./classwork/js/Dove.js ***!
  \******************************/
/*! exports provided: Dove */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Dove\", function() { return Dove; });\n/* harmony import */ var _Bird_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Bird.js */ \"./classwork/js/Bird.js\");\n\r\n\r\n\r\nclass Dove extends _Bird_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"] {\r\n    constructor(size, color) {\r\n     super(\"Голубь\", false, size, color);\r\n    }\r\n  \r\n    // - Летать\r\n    Fly (speed){\r\n      console.log(\"Скорость полета: \" + speed);\r\n    }\r\n  }\r\n\r\n  \n\n//# sourceURL=webpack:///./classwork/js/Dove.js?");

/***/ }),

/***/ "./classwork/js/Owl.js":
/*!*****************************!*\
  !*** ./classwork/js/Owl.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Owl; });\n/* harmony import */ var _Bird_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Bird.js */ \"./classwork/js/Bird.js\");\n\r\n\r\nclass Owl extends _Bird_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"] {\r\n    constructor(size, color) {\r\n      super(\"Сова\", true, size, color);\r\n    }\r\n    //- Охотиться\r\n    Hunt(object) {\r\n      console.log(\"Объект охоты: \" + object);\r\n    }\r\n    // - Летать\r\n    Fly(speed) {\r\n      console.log(\"Скорость полета: \" + speed);\r\n    }\r\n  }\n\n//# sourceURL=webpack:///./classwork/js/Owl.js?");

/***/ })

/******/ });
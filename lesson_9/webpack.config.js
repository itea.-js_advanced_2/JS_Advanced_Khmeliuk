const path = require('path');

module.exports = {
  entry: './classwork/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  }
};
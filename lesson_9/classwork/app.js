
import {Dove} from './js/Dove';
import Owl from './js/Owl.js';

let owl = new Owl(20, "grey");
owl.Hunt("мышь");
owl.Fly(20);
owl.Eat(15);
owl.Scurry();
owl.Sing("уу-уу");

console.log("");

let dove = new Dove(10, "grey");
dove.Fly(15);
dove.Eat(45);
dove.Scurry();
dove.Sing("ур-ур-ур");

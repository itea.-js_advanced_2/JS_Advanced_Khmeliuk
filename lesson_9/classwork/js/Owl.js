import Bird from './Bird.js';

export default class Owl extends Bird {
    constructor(size, color) {
      super("Сова", true, size, color);
    }
    //- Охотиться
    Hunt(object) {
      console.log("Объект охоты: " + object);
    }
    // - Летать
    Fly(speed) {
      console.log("Скорость полета: " + speed);
    }
  }
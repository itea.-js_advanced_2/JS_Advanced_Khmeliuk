export default class Bird {
    constructor(kind, isPredator, size, color) {
      this.isPredator = isPredator;
      this.color = color;
      this.kind = kind;
      this.size = size;
    }
  
    // - Нестись
    Scurry() {
      console.log(`Опа, яйцо`);
    }
    // //- Плавать
    // Swim = () => {
  
    // }
    //- Кушать
    Eat(eat) {
      if (eat > 20) {
        console.log(`птица сыта и больше есть не может!`);
      }
      else {
        console.log(`ням ням, было вкусно!`);
      }
    }
    //- Петь
    Sing(sound) {
      console.log(`${this.kind} издает ${sound}`);
    }
    // //- Переносить почту
    // SendMail = () => {
  
    // }
    // //- Бегать
    // Run = () => {
  
    // }
  }
/*

  Задание:
    Написать конструктор обьекта. Отдельные функции разбить на модули
    и использовать внутри самого конструктора.
    Каждую функцию выделить в отдельный модуль и собрать.

    Тематика - птицы.
    Птицы могут:
      - Нестись
      - Летать
      - Плавать
      - Кушать
      - Охотиться
      - Петь
      - Переносить почту
      - Бегать

  Составить птиц (пару на выбор, не обязательно всех):
      Страус
      Голубь
      Курица
      Пингвин
      Чайка
      Ястреб
      Сова

 */

class Bird {
  constructor(kind, isPredator, size, color) {
    this.isPredator = isPredator;
    this.color = color;
    this.kind = kind;
    this.size = size;
  }

  // - Нестись
  Scurry = () => {
    console.log(`Опа, яйцо`);
  }
  // //- Плавать
  // Swim = () => {

  // }
  //- Кушать
  Eat = (eat) => {
    if (eat > 20) {
      console.log(`птица сыта и больше есть не может!`);
    }
    else {
      console.log(`ням ням, было вкусно!`);
    }
  }
  //- Петь
  Sing = (sound) => {
    console.log(`${this.kind} издает ${sound}`);
  }
  // //- Переносить почту
  // SendMail = () => {

  // }
  // //- Бегать
  // Run = () => {

  // }
}

class Owl extends Bird {
  constructor(size, color) {
    super("Сова", true, size, color);
  }
  //- Охотиться
  Hunt = (object) => {
    console.log("Объект охоты: " + object);
  }
  // - Летать
  Fly = (speed) => {
    console.log("Скорость полета: " + speed);
  }
}

class Dove extends Bird {
  constructor(size, color) {
    super("Голубь", false, size, color);
  }

  // - Летать
  Fly = (speed) => {
    console.log("Скорость полета: " + speed);
  }
}

let owl = new Owl(20, "grey");
owl.Hunt("мышь");
owl.Fly(20);
owl.Eat(15);
owl.Scurry();
owl.Sing("уу-уу");

let dove = new Dove(10, "grey");
dove.Fly(15);
dove.Eat(45);
dove.Scurry();
dove.Sing("ур-ур-ур");

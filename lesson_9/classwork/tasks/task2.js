/*

Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678

 */
document.addEventListener('DOMContentLoaded', () => {
  //Задание 1:
  const div = document.getElementById("div");
  const btnRandomColor = document.getElementById("btnRandomColor");
  btnRandomColor.addEventListener('click', InsertColorToLocalStorage);

  let loadedColor = localStorage.getItem("color");
  div.style.background = loadedColor;

  function InsertColorToLocalStorage() {
    let color = getColorHex();
    div.style.background = color;
    console.log(color);
    window.localStorage.setItem("color", color);
  }

  function getColorHex() {

    let r = getRandomIntInclusive(0, 255);
    let g = getRandomIntInclusive(0, 255);
    let b = getRandomIntInclusive(0, 255);
    let code = "#" + fullColorHex(r, g, b);
    return code;
  }

  function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  let fullColorHex = function (r, g, b) {
    let red = rgbToHex(r);
    let green = rgbToHex(g);
    let blue = rgbToHex(b);
    return red + green + blue;
  };

  let rgbToHex = function (rgb) {
    let hex = Number(rgb).toString(16);
    if (hex.length < 2) {
      hex = "0" + hex;
    }
    return hex;
  };


  //Задание 2:
  const form = document.getElementById("form_id");
  let userCredentialLoaded = localStorage.getItem("userCredential");
  form.onsubmit = function (e) {
    e.preventDefault();

    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    let userCredential = {};
    userCredential.username = username;
    userCredential.password = password;
    console.log(userCredential);

    if ( userCredentialLoaded === JSON.stringify(userCredential)) {
      alert(`Привет ${username}`);
    }
    else{
      console.log(userCredential, userCredentialLoaded);
      localStorage.setItem("userCredential", JSON.stringify(userCredential));
    }
  }

})

//let imageUrls = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
import { imageUrls } from './myArray.js'
export class Modal {
    constructor() {
        this.modalDiv = document.createElement("div");
        this.onClickNext = function(){};
        this.onClickPrev = function(){};
        this.image = document.createElement("img");
        this.render();
        this.show;
        this.index = 0;
    }

    render() {
        this.modalDiv.classList.add("modal");
        let divWrapper = document.createElement("div");
        this.modalDiv.appendChild(divWrapper);

        let btnClose = document.createElement("a");
        btnClose.id = "boxclose";
        btnClose.classList.add("boxclose");
        btnClose.onclick = (e) => {
            this.modalDiv.style.display = "none";
        }

        let divImage = document.createElement("div");
        divImage.classList.add("divOneImage");
        divImage.id = "divOneImage";
        let btnPrev = document.createElement("button");
        btnPrev.innerText = "<";
        btnPrev.addEventListener('click', () => {
            this.onClickPrev();
            this.prevClick();
        });
        let btnNext = document.createElement("button");
        btnNext.innerText = ">";
        btnNext.addEventListener('click', () => {
            this.onClickNext();
            this.nextClick();
        });

        divImage.appendChild(btnClose);
        divImage.appendChild(btnPrev);
        divImage.appendChild(this.image);
        divImage.appendChild(btnNext);

        divWrapper.appendChild(divImage);
    }

    show(index) {
        this.index = index;
        this.image.src = imageUrls[this.index];
        this.modalDiv.style.display = "block";
        return this.modalDiv;
    }

    close() {
        this.modalDiv.style.display = "none";
    }

    nextClick() {
        this.image.src = imageUrls[this.index];
    }

    prevClick() {
        this.image.src = imageUrls[this.index];
    }
}

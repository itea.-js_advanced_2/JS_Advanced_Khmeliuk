
//let imageUrls = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
export class Slider {
    constructor() {
        this.divImage = document.createElement('div');
        this.modal = new Modal();
        this.nextClick = this.nextClick.bind(this);
        this.prevClick = this.prevClick.bind(this);
        this.modal.onClickNext = this.nextClick;
        this.modal.onClickPrev = this.prevClick;

        this.redrawImage = this.redrawImage.bind(this);
    }

    render() {
        this.divImage.classList.add("container");
        let btnPrev = document.createElement('button');
        btnPrev.id = "btnPrev";
        btnPrev.addEventListener('click', () => { this.prevClick() });
        btnPrev.innerHTML = "<";
        let btnNext = document.createElement('button');
        btnNext.id = "btnNext";
        btnNext.addEventListener('click', () => { this.nextClick() });
        btnNext.innerHTML = ">";
        this.divImage.appendChild(btnPrev);
        this.divImage.appendChild(btnNext);

        this.createImages(btnNext);

        this.redrawImage();

        document.body.appendChild(this.divImage);
    }

    createImages(btnNext) {
        let counter = 0;
        return imageUrls.forEach(() => {
            let img = document.createElement("img");
            img.name = counter;
            img.addEventListener('click', (e) => { this.imageClick(e) });
            this.divImage.insertBefore(img, btnNext);
            counter++;
        })
    }

    redrawImage() {
        let imageList = this.divImage.querySelectorAll("img");
        console.log("redrawImage()");

        for (var i = 0; i < imageList.length; i++) {
            imageList[i].src = imageUrls[i];
        }
    }

    prevClick() {
        console.log("slider.prevClick");
        let first = imageUrls.shift();
        imageUrls.push(first);
        this.redrawImage();
    }

    nextClick() {
        let first = imageUrls.pop();
        imageUrls.unshift(first);
        this.redrawImage();
    }

    imageClick(e) {
        document.body.appendChild(this.modal.show(e.target.name));
    }

    // window.onclick = function (event) {
    //     if (event.target.class == modal) {
    //         modal.style.display = "none";
    //     }
    // }
}
import { Modal } from './modal'
import { imageUrls } from './myArray.js'
